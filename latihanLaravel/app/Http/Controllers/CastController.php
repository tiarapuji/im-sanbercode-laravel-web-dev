<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create() {
        return view('cast.tambah');
    }

    public function store(Request $request) {
        // Validasi
        $request->validate([
            'name' => 'required',
            'age' => 'required',
            'bio' => 'required',
        ]);

        // Tambah data ke database
        DB::table('cast')->insert([
            'name' => $request->input('name'),
            'age' => $request->input('age'),
            'bio' => $request->input('bio'),
        ]);
        return redirect('/cast');
    }

    public function index() {
        $cast = DB::table('cast')->get();
        return view('cast.tampil', ['cast' => $cast]);
    
    }

    public function show($id){
        // dd($id);
        $cast = DB::table('cast')->find($id);
        return view('cast.detail', ['cast'=>$cast]);
    }

    public function edit($id) {
        $cast = DB::table('cast')->find($id);
        return view('cast.edit', ['cast'=>$cast]);
    }

    public function update($id, Request $request){
        // Validasi
        $request->validate([
            'name' => 'required',
            'age' => 'required',
            'bio' => 'required',
        ]);

        // Update
        $affected = DB::table('cast')
              ->where('id', $id)
              ->update([
                  'name' => $request->input('name'),
                  'age' => $request->input('age'),
                  'bio' => $request->input('bio'),
                ]
            );
        return redirect('/cast');
        
    }

    public function destroy($id) 
    {
        DB::table('cast')->where('id',$id)->delete();
        return redirect('/cast');
    }
}
