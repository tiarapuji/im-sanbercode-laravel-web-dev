<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('page.register');
    }

    public function welcome(Request $request)
    {
        $nameFirst = $request->input('fname');
        $nameLast = $request->input('lname');

        return view('welcome', ['nameFirst' => $nameFirst, 'nameLast' => $nameLast]);
    }
}