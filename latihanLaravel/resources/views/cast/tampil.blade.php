@extends('layouts.master')

@section('judul')
    Halaman Tambah Cast
@endsection

@section('content')

<a href="/cast/create" class="btn btn-primary btn-sn my-3" >Tambah</a>


<table class="table">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
    @forelse ($cast as $index => $pemeran)
  <tbody>
    <tr>
      <th scope="row">{{$index + 1}}</th>
      <td>{{$pemeran->name}}</td>
      <td>
      <form action="/cast/{{$pemeran->id}}" method="POST">
        <a href="/cast/{{$pemeran->id}}" class="btn btn-info btn-sm">Detail</a>
        <a href="/cast/{{$pemeran->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
            @method('delete')
            @csrf
            <input type="submit" value="Delete" class="btn btn-danger btn-sm">
      </form>
      </td>
    </tr>
    @empty
    <p>Tidak ada cast</p>
  </tbody>
    @endforelse
</table>
@endsection