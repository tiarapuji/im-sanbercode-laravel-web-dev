@extends('layouts.master')

@section('judul')
    Halaman Detail Cast
@endsection

@section('content')
<h3>{{$cast->name}}</h3>
<p>Seorang pemeran yang berusia {{$cast->age}}</p>
<p>{{$cast->bio}}</p>

@endsection