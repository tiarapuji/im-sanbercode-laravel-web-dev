@extends('layouts.master')

@section('judul')
    Halaman Tambah Cast
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('put')
    <!-- validation -->
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


  <div class="form-group">
    <label for="exampleInputEmail1">Nama Cast</label>
    <input type="tex" class="form-control" name="name" value="{{$cast->name}}">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Umur</label>
    <input type="text" class="form-control" name="age" value="{{$cast->age}}">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Bio</label>
    <input type="text" class="form-control" name="bio" value="{{$cast->bio}}">
  </div>
  <button type="submit" class="btn btn-warning">Update</button>
</form>
@endsection
    
