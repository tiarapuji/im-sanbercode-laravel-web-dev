@extends('layouts.master')

@section('judul')
    Halaman Tambah Cast
@endsection

@section('content')
<form action="/cast" method="POST">
    @csrf
    <!-- validation -->
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


  <div class="form-group">
    <label for="exampleInputEmail1">Nama Cast</label>
    <input type="tex" class="form-control" name="name">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Umur</label>
    <input type="text" class="form-control" name="age">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Bio</label>
    <input type="text" class="form-control" name="bio">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
    
