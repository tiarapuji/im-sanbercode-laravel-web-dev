@extends('layouts.master')

@section('judul')
    Halaman Daftar
@endsection

@section('content')
    <form action="/welcome" method="POST">
        @csrf
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>
        <label>First name:</label> <br><br>
        <input type="text" name=fname><br>
        <br>
        <label>Last name:</label> <br><br>
        <input type="text" name=lname><br>
        <br>
        <label>Gender:</label> <br><br>
        <input type="radio" name="jk">Male<br>
        <input type="radio" name="jk">Female<br>
        <input type="radio" name="jk">Other<br>
        <br>
        <label>Nationality:</label><br><br>
        <select name="nationality">
            <option value="1">Indonesian</option>
            <option value="2">Malaysian</option>
            <option value="3">Australian</option>
            <option value="4">Singaporean</option>
        </select><br>
        <br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="language" value="1">Bahasa Indonesia<br>
        <input type="checkbox" name="language" value="2">English<br>
        <input type="checkbox" name="language" value="3">Other<br><br>
        <label>Bio:</label><br>
        <br>
        <textarea name="bio" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up">
    @endsection